from telegram.ext import Updater

import logging

import settings
from core import Core

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main():
    updater = Updater(token=settings.TELEGRAM_TOKEN)
    dispatcher = updater.dispatcher

    logger.info('Updater initialized')

    core = Core(updater, dispatcher)
    logger.info('Handlers initialized')
    core.initialize()
    logger.info('Running...')


if __name__ == '__main__':
    main()
