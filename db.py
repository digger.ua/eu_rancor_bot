import json
import logging

import redis

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


# todo: setex 2 weeks for matches

class Database:
    client = None  # type: redis.Redis

    _namespace = None

    def __init__(self, client, namespace):
        self.client = client
        self._namespace = str(namespace)

    def ns(self, key):
        return '{}:{}'.format(self._namespace, key)


class UserDatabase(Database):

    def key(self, user_id):
        return self.ns('user:{}'.format(user_id))

    def alias_key(self, user_id):
        return self.ns('alias:{}'.format(user_id))

    def add(self, user_id, full_name):
        # todo: setex
        return self.client.set(self.key(user_id), full_name)

    def alias(self, user_id, alias):
        return self.client.set(self.alias_key(user_id), alias)

    def get_alias(self, user_id):
        return self.client.get(self.alias_key(user_id))