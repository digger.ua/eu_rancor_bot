import logging
from collections import defaultdict
from decimal import Decimal

from telegram.user import User

from db import Database
from enums import Phases
from lib import jsonutils, botutils

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Controller(Database):
    def __init__(self, client, namespace=None, users_db=None):
        super().__init__(client, namespace)

        self.users_db = users_db

    @property
    def current_phase(self):
        return self.client.get(self.ns('current_phase')) or Phases.P1

    @property
    def phase_damage(self):
        return int(self.client.get(
            self.ns('damage_' + self.current_phase)) or 0)

    @phase_damage.setter
    def phase_damage(self, val):
        k = self.ns('damage_' + self.current_phase)
        self.client.set(k, int(val))

    def _get_phase_key(self, phase=None):
        if not phase:
            return 'phase_{}'.format(self.current_phase)
        return 'phase_{}'.format(phase)

    def phase(self, num) -> str:
        num = str(num)
        if num not in Phases.all():
            return self.current_phase

        self.client.set(self.ns('current_phase'), num)
        phase_damage = self.client.get(self.ns('damage_' + num)) or 0
        self.client.set(self.ns('damage_' + num), phase_damage)
        self.phase_damage = int(phase_damage)
        return num

    def reset(self):
        phases = [self.ns('phase_{}'.format(p)) for p in Phases.all()]
        self.client.delete(*phases)

        damages = [self.ns('damage_' + p) for p in Phases.all()]
        self.client.delete(*damages)

        self.client.set(self.ns('current_phase'), Phases.P1)
        self.client.set(self.ns('damage_' + Phases.P1), 0)
        self.phase_damage = 0

    def set_damage(self, user: User, damage, *squad):
        damage = damage.replace(',', '.')
        damage = int(float(damage) * 100)

        name = self._get_phase_key()
        data = dict(
            damage=float(damage),
            squad=' '.join(squad) if squad else ''
        )

        prev_damage = float(self.client.get(self.ns('damage_' +
                                                    self.current_phase)))

        old_data = self.client.hget(self.ns(name), user.id)
        if old_data:
            self.remove_damage(user)
        elif prev_damage >= 10500.:
            return prev_damage / 100, False

        self.client.hset(self.ns(name), user.id, jsonutils.dumps(data))

        logger.info('[set_damage] user: {}. damage: {}'.format(user.name,
                                                               data['damage']))
        amount = self.update_total_damage(damage)
        return amount / 100, True

    def update_total_damage(self, damage: float):

        prev_damage = self.client.get(self.ns('damage_' + self.current_phase))
        amount = int(prev_damage or 0) + int(damage)
        self.client.set(self.ns('damage_' + self.current_phase), amount)
        logger.info('[total_damage] {}'.format(amount))
        return amount

    def remove_damage(self, user: User, phase=None, *args):
        if phase:
            phase = str(phase)
        name = self._get_phase_key(phase)

        data = self.client.hget(self.ns(name), user.id)
        data = jsonutils.loads(data)
        self.client.hdel(self.ns(name), user.id)
        logger.info('[remove_damage] user: {}. damage: {}'.format(
            user.name, data['damage']))
        self.client.decr(self.ns('damage_' + self.current_phase),
                         int(data['damage']))

    def get_summary(self, phase=None):
        phases = []
        if phase:
            phase = str(phase)

        if phase == 'all' or phase == 'players':
            phases = []
            for p in Phases.all():
                name = self._get_phase_key(p)
                response = self.client.hgetall(self.ns(name))
                users = [
                    dict(
                        user_id=user_id,
                        data=jsonutils.loads(data)
                    ) for user_id, data in response.items()
                ]
                phases.append(dict(
                    phase=p,
                    users=users
                ))
        elif phase in Phases.all() or not phase:
            name = self._get_phase_key()
            response = self.client.hgetall(self.ns(name))
            users = [
                dict(
                    user_id=user_id,
                    data=jsonutils.loads(data)
                ) for user_id, data in response.items()
            ]
            phases.append(dict(
                phase=phase or self.current_phase,
                users=users
            ))

        return phases, phase

    def summarize_table(self, bot, update, summary):
        data = []

        stats = {}

        for p in summary:
            for user in p['users']:
                member = self.get_member(bot, update, user['user_id'])

                if member.startswith('@'):
                    member = member[1:]

                user_data = user['data']
                user_damage = float(user_data['damage'] / 100)
                phase_stat = stats.setdefault(member, {})
                phase_stat.update({p['phase']: user_damage})

        total_stats = []
        total_stats.append(['username', 'Фаза 1', 'Фаза 2', 'Фаза 3',
                            'Фаза 4', 'Суммарно'])
        total_stats.append(['', '', '', '', '', ''])

        phase_totals = {'1': 0, '2': 0, '3': 0, '4': 0}
        total_players = 0
        for i, (player, stat) in enumerate(sorted(stats.items(), key=lambda
                x: sum(
            p for p in x[1].values()), reverse=True)):

            player_stat = ['{}) {}'.format(i + 1, player)]
            p_total = 0
            for p in ['1', '2', '3', '4']:
                p_damage = stat.get(p, 0)
                player_stat.append(
                    '{:.2f}'.format(p_damage) if p_damage else '')
                phase_totals[p] += p_damage
                p_total += p_damage

            player_stat.append('{:.2f}'.format(p_total))
            total_stats.append(player_stat)
            total_players += 1

        total_stats.append(['', '', '', '', '', ''])
        total_stats.append([
            '({}) общий по фазам'.format(total_players),
            '{:.2f}'.format(phase_totals['1']),
            '{:.2f}'.format(phase_totals['2']),
            '{:.2f}'.format(phase_totals['3']),
            '{:.2f}'.format(phase_totals['4']),
            ''
        ])
        return total_stats

    def summarize(self, bot, update, summary, phase):
        data = []

        if phase == 'players':
            return self.summarize_table(bot, update, summary)

        for p in summary:
            i = 0
            damage = []
            data.append(['Фаза ' + p['phase'], '', ''])
            data.append(['%', 'username', 'squad'])
            total_dmg = 0
            for user in p['users']:
                member = self.get_member(bot, update, user['user_id'])

                if member.startswith('@'):
                    member = member[1:]

                user_data = user['data']
                user_damage = float(user_data['damage'] / 100)
                damage.append([
                    '{:.2f}'.format(user_damage),
                    member,
                    user_data['squad']])
                total_dmg += user_damage
            data.append(['', '', ''])
            for dmg in sorted(damage, key=lambda x: x[0], reverse=True):
                i += 1
                data.append(['{}) {}'.format(i, dmg[0]), dmg[1], dmg[2]])
            data.append(['', '', ''])
            data.append(['{:.2f}'.format(total_dmg), 'Участников: %s' % i, ''])
            data.append(['', '', ''])
        return data

    def go(self, bot, update, summary, phase):
        data = []
        damage = []
        for p in summary:
            i = 0
            data.append(['Фаза ' + p['phase'], ''])
            data.append(['%', 'username'])
            total_dmg = 0
            for user in p['users']:
                member = self.get_member(bot, update, user['user_id'],
                                         alias=False)
                user_data = user['data']
                user_damage = float(user_data['damage'] / 100)
                damage.append([
                    '{:.2f}'.format(user_damage),
                    member])
                total_dmg += user_damage
            data.append(['', ''])
            for dmg in sorted(damage, key=lambda x: x[0], reverse=True):
                i += 1
                data.append(['{}) {}'.format(i, dmg[0]), dmg[1]])
            data.append(['', ''])
            data.append(['{:.2f}'.format(total_dmg), 'Участников: %s' % i])
            data.append(['', ''])
        return data

    def get_member(self, bot, update, user_id, alias=True):
        if alias:
            user_alias = self.users_db.get_alias(user_id)
            if user_alias:
                return user_alias

        return botutils.get_member(bot, update, user_id)
