import redis
import logging
import os
import functools
import locale

import settings
from controller import Controller
from handlers import RegexpCommandHandler
from lib import botutils

locale.setlocale(locale.LC_ALL, 'uk_UA')

from telegram.ext import CommandHandler, MessageHandler, Filters, RegexHandler
from telegram.parsemode import ParseMode

from db import UserDatabase
from messages import Messages

from settings import APP_ID, TELEGRAM_TOKEN, PORT
from tabulate import tabulate

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Core:
    def __init__(self, updater, dispatcher):
        self.updater = updater
        self.dispatcher = dispatcher

        self.client = redis.from_url(
            os.environ.get('REDIS_URL'),
            decode_responses=True
        )

        self.chat_id = None

        self.init_handlers()

    def init_hooks(func):
        @functools.wraps(func)
        def method(self, bot, update, *args, **kwargs):
            is_permitted = self.on_before_call(bot, update, *args, **kwargs)
            if not is_permitted:
                return
            result = func(self, bot, update, *args, **kwargs)
            self.on_after_call(bot, update, *args, **kwargs)
            return result

        return method

    def on_before_call(self, bot, update, *args, **kwargs):

        chat_id = str(update.message.chat_id)
        if chat_id not in settings.ALLOWED_CHATS:
            logger.info('Access from not allowed chat: {} [{}]'.format(
                update.message.chat.title, update.message.chat_id
            ))
            return False

        if self.chat_id is None:
            self.chat_id = chat_id
            self.users = UserDatabase(self.client, chat_id)
            self.controller = Controller(self.client, chat_id, self.users)

        self._save_user_to_db(bot, update)
        return True

    def on_after_call(self, bot, update, *args, **kwargs):
        self.chat_id = None
        self.users = None
        self.controller = None

    def _save_user_to_db(self, bot, update):
        user = update.message.from_user
        self.users.add(user.id, user.full_name)
        logger.info('User "{}" "{}" has been added'.format(
            user.id, user.full_name))

    def init_handlers(self):
        _start = CommandHandler('help', self._start)
        _reset = CommandHandler('reset', self._reset)
        _phase = CommandHandler('phase', self._phase, pass_args=True)

        _regex_register = RegexpCommandHandler('^\+([^\s]+)\s?(.+)?$',
                                               self._register_regex,
                                               pass_groups=True)

        _register = CommandHandler('+', self._register, pass_args=True)
        _unregister = CommandHandler('-', self._unregister, pass_args=True)
        _summary = CommandHandler('summary', self._summary, pass_args=True)
        _go = CommandHandler('go', self._go)
        _me = CommandHandler('me', self._me, pass_args=True)

        self.dispatcher.add_handler(_start)
        self.dispatcher.add_handler(_reset)
        self.dispatcher.add_handler(_phase)
        self.dispatcher.add_handler(_register)
        self.dispatcher.add_handler(_regex_register)
        self.dispatcher.add_handler(_unregister)
        self.dispatcher.add_handler(_summary)
        self.dispatcher.add_handler(_go)
        self.dispatcher.add_handler(_me)

    @init_hooks
    def _start(self, bot, update):
        hello_message = Messages.HELLO.format().strip()
        update.message.reply_text(
            hello_message,
            parse_mode=ParseMode.MARKDOWN,
            quote=False)

    @init_hooks
    def _reset(self, bot, update):
        self.controller.reset()
        update.message.reply_text(
            'Данные полностью сброшены. Запущена 1я фаза',
            parse_mode=ParseMode.MARKDOWN,
            quote=False)

    @init_hooks
    def _phase(self, bot, update, args):
        phase = self.controller.phase(args[0])
        update.message.reply_text(
            'Запущена фаза: {}'.format(phase),
            parse_mode=ParseMode.MARKDOWN,
            quote=False)

    @init_hooks
    def _register(self, bot, update, args):
        self.__do_handle_register(bot, update, *args)

    @init_hooks
    def _register_regex(self, bot, update, groups):
        damage = groups[0]
        squad = groups[1]
        if squad:
            self.__do_handle_register(bot, update, damage, squad)
        else:
            self.__do_handle_register(bot, update, damage)

    def __do_handle_register(self, bot, update, dmg, *args):
        user = update.message.from_user
        amount, registered = self.controller.set_damage(user, dmg, *args)

        if registered:
            update.message.reply_text(
                'Урон записан. Итого: {:.2f}'.format(amount),
                parse_mode=ParseMode.MARKDOWN,
            )
        else:
            update.message.reply_text(
                'Уже набрали: {:.2f}. Твой урон не записан'.format(amount),
                parse_mode=ParseMode.MARKDOWN,
            )

    @init_hooks
    def _unregister(self, bot, update, args):
        user = update.message.from_user
        self.controller.remove_damage(user, *args)
        update.message.reply_text(
            'Урон сброшен',
            parse_mode=ParseMode.MARKDOWN)

    @init_hooks
    def _summary(self, bot, update, args):
        summary, phase = self.controller.get_summary(*args)
        data = self.controller.summarize(bot, update, summary, phase)

        msg = tabulate(data, headers="firstrow")
        msg = '<pre>%s</pre>' % msg
        update.message.reply_text(
            msg,
            parse_mode=ParseMode.HTML)

    @init_hooks
    def _go(self, bot, update):
        summary, phase = self.controller.get_summary()
        data = self.controller.go(bot, update, summary, phase)
        logger.info('data %s' % data)
        msg = tabulate(data, headers="firstrow")
        logger.info('msg %s' % msg)
        msg = '<pre>%s</pre>' % msg
        update.message.reply_text(
            msg,
            parse_mode=ParseMode.HTML)

    @init_hooks
    def _me(self, bot, update, args):
        nick = ' '.join(args)
        user = update.message.from_user
        self.users.alias(user.id, nick)
        update.message.reply_text(
            'Приятно познакомиться, {}'.format(nick),
            parse_mode=ParseMode.MARKDOWN)

    init_hooks = staticmethod(init_hooks)

    def initialize(self):
        webhook_url = 'https://' + APP_ID + '.herokuapp.com/' + TELEGRAM_TOKEN
        self.updater.start_webhook(
            listen='0.0.0.0',
            port=PORT,
            url_path=TELEGRAM_TOKEN,
            webhook_url=webhook_url)
        self.updater.bot.set_webhook(webhook_url)
        self.updater.start_polling()

        self.updater.idle()
