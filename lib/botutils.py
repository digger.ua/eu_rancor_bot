def get_member(bot, update, user_id):
    try:
        member = bot.get_chat_member(update.message.chat_id, user_id)
    except Exception:
        full_name = user_id
    else:
        full_name = member.user.name

    return full_name
