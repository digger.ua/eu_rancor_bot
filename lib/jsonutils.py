import json
from datetime import date, datetime, timedelta
from decimal import Decimal


def json_serial(obj):
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()

    if isinstance(obj, timedelta):
        return str(obj)

    if isinstance(obj, Decimal):
        return '0' if obj == 0 else str(obj)

    if isinstance(obj, set):
        return list(obj)

    raise TypeError("Type %s not serializable" % type(obj))


def dumps(obj):
    return json.dumps(obj, default=json_serial)


def loads(s):
    return json.loads(s, parse_float=Decimal)
