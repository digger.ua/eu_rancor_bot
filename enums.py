class ImmutableEnum(type):
    def __setattr__(self, *args, **kwargs):
        raise NotImplementedError()


class Enum(metaclass=ImmutableEnum):
    name = None

    @classmethod
    def all(cls):
        props = dir(cls)
        vals = [getattr(cls, p) for p in props if not p.startswith('_')]
        return [v for v in vals if type(v) is str]


class Phases(Enum):
    P1 = '1'
    P2 = '2'
    P3 = '3'
    P4 = '4'
