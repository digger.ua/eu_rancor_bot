import os
from dateutil import rrule

APP_ID = os.environ.get('APP_ID')
TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
PORT = int(os.environ.get('PORT', '8443'))

ALLOWED_CHATS = (os.environ.get('ALLOWED_CHATS') or '').split(',')
